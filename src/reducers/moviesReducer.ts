import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "store/store";
import { StateType } from "types/types";

interface PayloadID {
  id: string;
}

const INITIAL_STATE: StateType = {
  movies: [],
};

export const moviesSlice = createSlice({
  name: "reducers",
  initialState: INITIAL_STATE,
  reducers: {
    getMovies: (state: StateType, action: PayloadAction<StateType>) => {
      state.movies = action.payload.movies;
    },
    toggleLikeMovie: (
      state: StateType,
      action: PayloadAction<PayloadID & { liked: boolean | undefined }>
    ) => {
      const toggleMutation = state.movies.map((item) => {
        if (item.id === action.payload.id) {
          if (action.payload.liked === undefined) {
            console.log("first");
            // startinf state
            if (item.disliked === true) {
              item.dislikes -= 1;
            }
            item.likes += 1;
            item.liked = true;
          } else if (action.payload.liked === true) {
            console.log("second");
            // like is already clicked
            item.likes -= 1;
            item.liked = false;
          } else if (action.payload.liked === false) {
            console.log("third");
            // its not clicked but not the first time
            item.likes += 1;
            item.liked = true;
            if (item.disliked === true) {
              item.dislikes -= 1;
            }
          }
        }
        return item;
      });

      state.movies = toggleMutation;
    },
    toggleDislikeMovie: (
      state: StateType,
      action: PayloadAction<
        PayloadID & {
          disliked: boolean | undefined;
        }
      >
    ) => {
      const toggleMutation = state.movies.map((item) => {
        if (item.id === action.payload.id) {
          if (action.payload.disliked === undefined) {
            //starting state
            item.disliked = true;
            item.dislikes += 1;
            if (item.liked === true) {
              item.likes -= 1;
            }
          } else if (action.payload.disliked === true) {
            // when its already disliked
            item.dislikes -= 1;
            item.disliked = false;
          } else if (action.payload.disliked === false) {
            // when like button is clicked
            if (item.liked === true) {
              item.likes -= 1;
            }
            item.dislikes += 1;
            item.liked = true;
          }
        }
        return item;
      });

      state.movies = toggleMutation;
    },

    deleteMovie: (state: StateType, action: PayloadAction<PayloadID>) => {
      const deleteMutation = state.movies.filter(
        (el) => el.id !== action.payload.id
      );
      state.movies = deleteMutation;
    },
  },
});

export const { getMovies, toggleDislikeMovie, toggleLikeMovie, deleteMovie } =
  moviesSlice.actions;

export const selectMovies = (state: RootState) => state.movies;

export default moviesSlice.reducer;
