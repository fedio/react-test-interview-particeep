import React, { useEffect, useState } from "react";
import classes from "./root.module.scss";
import Header from "components/header/header";
import FilmCard from "components/filmCard/card";
import { FilterType, Movie } from "types/types";
import Multiselect from "multiselect-react-dropdown";
import { uniqBy } from "lodash";
import { useAppSelector, useAppDispatch } from "hooks/storeHooks";
import { sagaActions } from "saga/sagaActions";
import {
  deleteMovie,
  toggleDislikeMovie,
  toggleLikeMovie,
} from "reducers/moviesReducer";

const RootContainer = () => {
  const dispatch = useAppDispatch();
  const { movies } = useAppSelector((state) => state.movies);
  const [data, setData] = useState<Movie[]>([]);
  const [filters, setFilters] = useState<FilterType[]>([]);
  const [filteredData, setFilteredData] = useState<Movie[]>([]);

  useEffect(() => {
    dispatch({ type: sagaActions.GET_DATA_SAGA });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setData(movies);
    setFilteredData(movies);
    filterBuilder(movies);
  }, [movies]);

  const filterBuilder = (input: Movie[]) => {
    const allFilters = input
      .filter((el) => el.category)
      .map((item) => ({ name: item.category, id: item.id }));
    const uniqFilters = uniqBy(allFilters, "name");
    setFilters(uniqFilters);
  };

  const onSelect = (selectedList: FilterType[], selectedItem: FilterType) => {
    const filteredArray = data.filter((el) =>
      selectedList.some((e) => e.name === el.category)
    );
    setFilteredData(filteredArray);
  };
  const onRemove = (selectedList: FilterType[], selectedItem: FilterType) => {
    const filteredArray = data.filter((el) =>
      selectedList.some((e) => e.name === el.category)
    );
    setFilteredData(filteredArray);
    if (selectedList.length === 0) {
      setFilteredData(data);
    }
  };

  const handleDelete = (id: string) => {
    dispatch(deleteMovie({ id }));
  };

  const handleLike = (id: string, liked: boolean | undefined) => {
    dispatch(toggleLikeMovie({ id: id, liked: liked }));
  };
  const handleDislike = (id: string, disliked: boolean | undefined) => {
    dispatch(toggleDislikeMovie({ id: id, disliked: disliked }));
  };

  return (
    <div className={classes.container}>
      <Header title="Movies List" />
      <div className={classes.filterBar}>
        <Multiselect
          options={filters}
          onSelect={onSelect}
          onRemove={onRemove}
          displayValue="name"
        />
      </div>
      <div className={classes.list}>
        {filteredData.map((item) => {
          return (
            <FilmCard
              category={item.category}
              title={item.title}
              likes={item.likes}
              dislikes={item.dislikes}
              key={item.id}
              liked={item.liked}
              disliked={item.disliked}
              id={item.id}
              onDelete={() => handleDelete(item.id)}
              onLike={() => handleLike(item.id, item.liked)}
              onDislike={() => handleDislike(item.id, item.disliked)}
            />
          );
        })}
      </div>
    </div>
  );
};

export default RootContainer;
