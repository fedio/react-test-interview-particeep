import { all, fork } from "redux-saga/effects";
import rootSaga from "./moviesSaga";

const sagas = [rootSaga];
// combine multiple sagas
// eslint-disable-next-line import/no-anonymous-default-export
export default function* () {
  yield all(sagas.map((saga) => fork(saga)));
}
