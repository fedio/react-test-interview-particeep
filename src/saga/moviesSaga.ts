import { call, put, takeEvery } from "redux-saga/effects";
import { movies$ } from "data/movies";
import { getMovies } from "reducers/moviesReducer";
import { Movie } from "types/types";
import { sagaActions } from "./sagaActions";
//import { mutateData } from "utils/dataMutation";

function* getMoviesSaga() {
  try {
    let response: Movie[] = yield call(() => movies$);
    yield put(getMovies({ movies: response }));
  } catch (e) {
    console.log(e);
  }
}

export default function* rootSaga() {
  yield takeEvery(sagaActions.GET_DATA_SAGA, getMoviesSaga);
}
