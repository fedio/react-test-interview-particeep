export type Movie = {
  id: string;
  title: string;
  category: string;
  likes: number;
  dislikes: number;
  liked?: boolean;
  disliked?: boolean;
};

export type FilterType = {
  id: string;
  name: string;
};

export type StateType = {
  movies: Movie[];
};
