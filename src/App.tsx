import React from "react";
import store from "store/store";
import { Provider } from "react-redux";
import RootContainer from "containers/RootContainer";

function App() {
  return (
    <Provider store={store}>
      <RootContainer />
    </Provider>
  );
}

export default App;
