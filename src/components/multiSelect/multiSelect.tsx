import React from "react";
import { Movie } from "types/types";
import classes from "./multiSelect.module.scss";
import check from "assets/PNG/check.png";

interface Props<T> {
  label: string;
  data: T[];
  selectedData: T[];
  handleSelectedOption: (item: T) => void;
}

const MultiSelect: React.FC<Props<Movie>> = ({
  label,
  data,
  selectedData,
  handleSelectedOption,
}) => {
  return (
    <div className={classes.container}>
      <div>
        <span>{label}</span>
      </div>
      <div className={classes.selectContainer}>
        <select name="select category" className={classes.selectComponent}>
          {data.map((item) => {
            return (
              <option
                className={classes.selectedElement}
                onClick={() => handleSelectedOption(item)}
                value={item.id}
                key={item.id}
              >
                {item.category}
                <img src={check} alt="check" />
              </option>
            );
          })}
          <hr />
          {selectedData.map((element) => {
            return <option value={element.id}>{element.title}</option>;
          })}
        </select>
      </div>
    </div>
  );
};
export default MultiSelect;
