import React from "react";
import classes from "./pagination.module.scss";

interface Props {
  perPage: number;
  total: number;
  currentPage: number;
}

const Pagination: React.FC<Props> = ({ currentPage, perPage, total }) => {
  /* const pages = []; */

  return (
    <div className={classes.container}>
      <div className={classes.pages}></div>
    </div>
  );
};

export default Pagination;
