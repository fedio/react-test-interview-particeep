import React from "react";
import classes from "./button.module.scss";

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  title: string;
  hasIcon?: boolean;
  icon?: string;
  clicked?: boolean;
  liked?: boolean | undefined;
  disliked?: boolean | undefined;
}

const Button: React.FC<Props> = ({
  title,
  hasIcon,
  icon,
  clicked,
  liked,
  disliked,
  ...other
}) => {
  return (
    <button
      className={
        !liked
          ? classes.container
          : liked
          ? classes.containerLiked
          : disliked === true
          ? classes.containerDisliked
          : classes.container
      }
      {...other}
    >
      {hasIcon && (
        <div className={classes.iconWrapper}>
          <img className={classes.icon} src={icon} alt="delete" />
        </div>
      )}
      <span className={classes.text}>{title}</span>
    </button>
  );
};

export default Button;

Button.defaultProps = {
  hasIcon: false,
  icon: undefined,
  clicked: undefined,
};
