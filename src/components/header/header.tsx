import React from "react";
import classes from "./header.module.scss";

interface Props {
  title: string;
}

const Header: React.FC<Props> = ({ title }) => {
  return (
    <div className={classes.container}>
      <span className={classes.title}>{title}</span>
    </div>
  );
};

export default Header;
