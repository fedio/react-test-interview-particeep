import Button from "components/button/button";
import React from "react";
import classes from "./card.module.scss";

interface Props {
  id: string;
  title: string;
  likes: number;
  dislikes: number;
  category: string;
  liked?: boolean;
  disliked?: boolean;
  onDelete: (id: string) => void;
  onLike: (id: string, liked: boolean | undefined) => void;
  onDislike: (id: string, liked: boolean | undefined) => void;
}

const FilmCard: React.FC<Props> = ({
  id,
  title,
  likes,
  dislikes,
  category,
  liked,
  onDelete,
  onLike,
  onDislike,
  disliked,
}) => {
  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <span className={classes.title}>Title: {title}</span>
      </div>
      <div className={classes.body}>
        <div className={classes.category}>Category :{category}</div>
        <div className={classes.ratio}> likes : {likes}</div>
        <div className={classes.ratio}> Dislikes : {dislikes}</div>
      </div>
      <div className={classes.actionPart}>
        <Button
          title="like"
          clicked={liked}
          onClick={() => onLike(id, liked)}
        />
        <Button
          title="dislike"
          clicked={disliked}
          onClick={() => onDislike(id, disliked)}
        />
        <Button title="Delete" onClick={() => onDelete(id)} />
      </div>
    </div>
  );
};
export default FilmCard;
