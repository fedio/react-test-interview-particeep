import { configureStore } from "@reduxjs/toolkit";
import { Middleware } from "redux";
import { createLogger } from "redux-logger";
import createSagaMiddleware, { SagaIterator, SagaMiddleware } from "redux-saga";
import rootReducer from "reducers";
import RootSaga from "saga/index";

const middlewares: Middleware[] = [];

let sagaMiddelWare: SagaMiddleware<SagaIterator> = createSagaMiddleware();
middlewares.push(sagaMiddelWare);

if (process.env.NODE_ENV === "development") {
  const logger = createLogger();
  middlewares.push(logger);
}

// store --------------------------------------------
export const store = configureStore({
  reducer: rootReducer,
  middleware: [...middlewares],
});

sagaMiddelWare.run(RootSaga);

export default store;

// types --------------------------------------------
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
