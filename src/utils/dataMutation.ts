import { Movie } from "types/types";

export const mutateData = (data: Movie[]) => {
  const mutation = data.map((el) => {
    el.liked = undefined;
    return el;
  });
  return mutation;
};
